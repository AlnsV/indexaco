# IndexACO

# Tipos de grafos soportados:
*  hypercube
*  star
*  wheel
*  debruijn
*  cycle

# Tamaño de grafos:
*  small
*  medium
*  big

# Modos de ejecución
* Secuencial
* Multihilos


# Como correr el programa
dentro del directorio ejecutar:
*  `go get -d -v ./... && go install -v ./...` dependencias
*  `go build -o indexaco .` compilación
*  `./indexaco --graph_type hypercube --size small --sequentially true --test_iters 3` ejecución
* 

# Casos de pruebas

Ubicados en el directorio `cases`

NOTA: el repositorio debe estar dentro del GOPATH de lo contrario go no sabrá como buscarlo.