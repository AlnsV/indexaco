package main

import "github.com/AlnsV/graph"

type Ant struct {
	Vertex            int
	VertexPath        []int
	Solution          Routing
	LinksTimesVisited map[Link]int
}

func NewAnt(InitialVtx int) *Ant {
	return &Ant{
		Vertex:            InitialVtx,
		VertexPath:        []int{InitialVtx},
		LinksTimesVisited: make(map[Link]int)}
}

// O(N)
func LoadNodesToAnts(g *graph.Mutable) []Ant {
	var Ants []Ant

	for v := 0; v < g.Order(); v++ {
		g.Visit(v, func(w int, c int64) (skip bool) {
			Ants = append(Ants, *NewAnt(v))
			return true
		})
	}
	return Ants
}

type Link struct {
	From int
	To   int
}

type AntLinks struct {
	Links     map[Link]int
	LinkCount int
}

func NewAntLinks(links map[Link]int) *AntLinks {
	return &AntLinks{Links: links, LinkCount: len(links)}
}
