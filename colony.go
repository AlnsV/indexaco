package main

type Colony struct {
	Alpha float64 // Importancia feromonas
	Beta  float64 // Importancia visibilidad
	Pho   float64 // Peso del paso del tiempo
	Q     float64 // Constante

	Ants []Ant
}

func NewColony(a, b, p, q float64) *Colony {
	return &Colony{
		Alpha: a,
		Beta:  b,
		Pho:   p,
		Q:     q,
	}
}
