
nohup ./indexaco --graph_type hypercube --size opt --sequentially true --test_iters 3 --type 1> hp_opt.log &
nohup ./indexaco --graph_type hypercube --size medium --sequentially true --test_iters 3 > hp_medium.log &
nohup ./indexaco --graph_type hypercube --size big --sequentially true --test_iters 3 > hp_big.log &

nohup ./indexaco --graph_type star --size opt --sequentially true --test_iters 3 > star_opt.log &
nohup ./indexaco --graph_type star --size medium --sequentially true --test_iters 3 > star_medium.log &
nohup ./indexaco --graph_type star --size big --sequentially true --test_iters 3 > star_big.log &

nohup ./indexaco --graph_type wheel --size small --sequentially true --test_iters 3 > wh_small.log &
nohup ./indexaco --graph_type wheel --size medium --sequentially true --test_iters 3 > wh_medium.log &
nohup ./indexaco --graph_type wheel --size big --sequentially true --test_iters 3 > wh_big.log &

nohup ./indexaco --graph_type debruijn --size small --sequentially true --test_iters 3 > db_small.log &
nohup ./indexaco --graph_type debruijn --size medium --sequentially true --test_iters 3 > db_medium.log &
nohup ./indexaco --graph_type debruijn --size big --sequentially true --test_iters 3 > db_big.log &

nohup ./indexaco --graph_type cycle --size small --sequentially true --test_iters 3 > cy_small.log &
nohup ./indexaco --graph_type cycle --size medium --sequentially true --test_iters 3 > cy_medium.log &
nohup ./indexaco --graph_type cycle --size big --sequentially true --test_iters 3 > cy_big.log &

nohup ./indexaco --graph_type hypercube --types 1 --size opt --sequentially true --test_iters 3 > hp_opt.log &
nohup ./indexaco --graph_type hypercube --types 2 --size opt --sequentially true --test_iters 3 > hp_opt.log &
nohup ./indexaco --graph_type hypercube --types 3 --size opt --sequentially true --test_iters 3 > hp_opt.log &
nohup ./indexaco --graph_type hypercube --types 4 --size opt --sequentially true --test_iters 3 > hp_opt.log &
nohup ./indexaco --graph_type hypercube --types 5 --size opt --sequentially true --test_iters 3 > hp_opt.log &


nohup ./indexaco --graph_type star --types 1 --size opt --sequentially true --test_iters 3 > star_opt.log &
nohup ./indexaco --graph_type star --types 2 --size opt --sequentially true --test_iters 3 > star_opt.log &
nohup ./indexaco --graph_type star --types 3 --size opt --sequentially true --test_iters 3 > star_opt.log &
nohup ./indexaco --graph_type star --types 4 --size opt --sequentially true --test_iters 3 > star_opt.log &
nohup ./indexaco --graph_type star --types 5 --size opt --sequentially true --test_iters 3 > star_opt.log &



nohup ./indexaco --graph_type debruijn --types 1 --size opt --sequentially true --test_iters 3 > db_opt.log &
nohup ./indexaco --graph_type debruijn --types 2 --size opt --sequentially true --test_iters 3 > db_opt.log &
nohup ./indexaco --graph_type debruijn --types 3 --size opt --sequentially true --test_iters 3 > db_opt.log &
nohup ./indexaco --graph_type debruijn --types 4 --size opt --sequentially true --test_iters 3 > db_opt.log &
nohup ./indexaco --graph_type debruijn --types 5 --size opt --sequentially true --test_iters 3 > db_opt.log &

