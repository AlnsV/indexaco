package main

import (
	"sort"
)

func CompareSolutions(CurrentSolution, NewSolution []int) []int {
	sort.Slice(NewSolution, func(i, j int) bool {
		return NewSolution[i] > NewSolution[j]
	})
	var NewCurrentSol []int
	NewCurrentSol = NewSolution

	if len(CurrentSolution) == 0 || len(CurrentSolution) != len(NewSolution) {
		return NewCurrentSol
	}

	for idx, val := range NewSolution {
		if val < CurrentSolution[idx] {
			return NewCurrentSol
		}
	}
	NewCurrentSol = CurrentSolution
	return NewCurrentSol
}
