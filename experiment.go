package main

import "fmt"

type TestCase struct {
	Name          string
	MaxIterations int
	Alpha         float64
	Beta          float64
	Pho           float64
	Q             float64
	gType         string
	gSize         string
	Sequentially  bool
}

func BuildCases(seq bool, graphType string, size string, alt int) []*TestCase {
	var vertex, names []string
	switch graphType {
	case "cycle", "star", "wheel":
		names = []string{graphType}
		switch size {
		case "small":
			vertex = []string{"3", "4", "5", "6", "7", "8", "9"}
		case "medium":
			vertex = []string{"10", "20", "30", "40", "50", "60", "70", "80", "90"}
		case "big":
			vertex = []string{
				"100", "200", "300", "400", "500", "600", "700", "800", "900", "1000",
			}
		case "opt":
			vertex = []string{
				"100",
			}
		case "all":
			vertex = []string{
				"3", "4", "5", "6", "7", "8", "9", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100",
				"200", "300", "400", "500", "600", "700", "800", "900", "1000",
			}
		}
	case "hypercube":
		names = []string{"hypercube"}
		switch size {
		case "small":
			vertex = []string{"4", "8"}
		case "medium":
			vertex = []string{"16", "32", "64"}
		case "big":
			vertex = []string{"128", "256", "512", "1024"}
		case "all":
			vertex = []string{
				"4", "8", "16", "32", "64", "128", "256", "512", "1024",
			}
		case "opt":
			vertex = []string{
				"128",
			}
		}

	case "debruijn":
		names = []string{"debruijn"}
		switch size {
		case "small":
			vertex = []string{"3", "4", "5", "6", "7", "8", "9"}
		case "medium":
			vertex = []string{"10", "20", "27", "32", "49", "64", "81"}
		case "big":
			vertex = []string{"100", "121", "216", "343", "400", "512", "625", "729", "1000"}
		case "all":
			vertex = []string{
				"3", "4", "5", "6", "7", "8", "9", "10", "20", "27", "32", "49", "64", "81",
				"100", "121", "216", "343", "400", "512", "625", "729", "1000",
			}
		case "opt":
			vertex = []string{
				"100",
			}
		}
	case "random":
		names = []string{"random"}
		switch size {
		case "small":
			vertex = []string{"4", "8"}
		case "medium":
			vertex = []string{"16", "32", "64"}
		case "big":
			vertex = []string{"128", "256", "512", "1024"}
		case "all":
			vertex = []string{
				"4", "8", "16", "32", "64", "128", "256", "512", "1024",
			}
		}
	}
	// Constante Q
	maxIters := []int{3}     // Cantidad de iteraciones
	Alphas := []float64{5.0} // Importancia de las feromonas
	Betas := []float64{1.0}  // Importancia de la visibilidad (En este caso es 1/(veces por la que pasa por X enlaces))
	Phos := []float64{0.5}   // Factor de evaporacion de las feromonas
	Q := []float64{2.0}
	switch alt {
	case 1:
		maxIters = []int{3}     // Cantidad de iteraciones
		Alphas = []float64{5.0} // Importancia de las feromonas
		Betas = []float64{1.0}  // Importancia de la visibilidad (En este caso es 1/(veces por la que pasa por X enlaces))
		Phos = []float64{0.5}   // Factor de evaporacion de las feromonas
		Q = []float64{2.0}
	case 2:
		maxIters = []int{3}     // Cantidad de iteraciones
		Alphas = []float64{4.0} // Importancia de las feromonas
		Betas = []float64{2.0}  // Importancia de la visibilidad (En este caso es 1/(veces por la que pasa por X enlaces))
		Phos = []float64{0.5}   // Factor de evaporacion de las feromonas
		Q = []float64{2.0}
	case 3:
		maxIters = []int{3}     // Cantidad de iteraciones
		Alphas = []float64{3.0} // Importancia de las feromonas
		Betas = []float64{3.0}  // Importancia de la visibilidad (En este caso es 1/(veces por la que pasa por X enlaces))
		Phos = []float64{0.5}   // Factor de evaporacion de las feromonas
		Q = []float64{2.0}
	case 4:
		maxIters = []int{3}     // Cantidad de iteraciones
		Alphas = []float64{2.0} // Importancia de las feromonas
		Betas = []float64{4.0}  // Importancia de la visibilidad (En este caso es 1/(veces por la que pasa por X enlaces))
		Phos = []float64{0.5}   // Factor de evaporacion de las feromonas
		Q = []float64{2.0}
	case 5:
		maxIters = []int{3}     // Cantidad de iteraciones
		Alphas = []float64{1.0} // Importancia de las feromonas
		Betas = []float64{5.0}  // Importancia de la visibilidad (En este caso es 1/(veces por la que pasa por X enlaces))
		Phos = []float64{0.5}   // Factor de evaporacion de las feromonas
		Q = []float64{2.0}
	}
	var cases []*TestCase
	for _, name := range names {
		for _, vert := range vertex {
			for _, maxIter := range maxIters {
				for _, alpha := range Alphas {
					for _, beta := range Betas {
						for _, pho := range Phos {
							for _, q := range Q {
								cases = append(cases, &TestCase{
									fmt.Sprintf("%s con %s vertices y %d iteraciones", name, vert, maxIter),
									maxIter,
									alpha,
									beta,
									pho,
									q,
									name,
									vert,
									seq,
								})
							}
						}
					}
				}
			}
		}
	}
	return cases
}
