package main

import (
	"bufio"
	"github.com/AlnsV/graph"
	"log"
	"os"
	"strconv"
	"strings"
)

// LoadStructureFromFile loads a new graph and the structures needed for the algorithm
func LoadStructureFromFile(fileType, graphType string, size string) (*graph.Mutable, map[Link]int, map[Link]float64, error) {
	links := map[Link]int{}
	pheromoneLinks := map[Link]float64{}
	gSize, err := strconv.Atoi(size)
	if err != nil {
		return &graph.Mutable{}, map[Link]int{}, map[Link]float64{}, err
	}
	g := graph.New(gSize)
	read := false
	f, err := os.Open("cases/" + graphType + "/" + size + fileType)
	if err != nil {
		return &graph.Mutable{}, map[Link]int{}, map[Link]float64{}, err
	}
	defer func() {
		if err = f.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	s := bufio.NewScanner(f)
	for s.Scan() {
		line := s.Text()
		if read {
			arc := strings.Split(line, " ")
			org, _ := strconv.Atoi(arc[0])
			dest, _ := strconv.Atoi(arc[1])
			if org > dest {
				org, dest = dest, org
			}
			g.AddBoth(org, dest)
			links[Link{org, dest}] = 0
			pheromoneLinks[Link{org, dest}] = 1.0
		}
		if line == "#" {
			read = true
		}
	}
	err = s.Err()
	if err != nil {
		return &graph.Mutable{}, map[Link]int{}, map[Link]float64{}, err
	}
	return g, links, pheromoneLinks, nil
}
