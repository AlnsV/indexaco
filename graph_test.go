package main

import (
	"fmt"
	"testing"
)

func TestLoadStructureFromFile(t *testing.T) {
	grap, _, _, err := LoadStructureFromFile(".in", "hiperc4")
	if err != nil {
		t.Error(err)
	}
	fmt.Printf("Loaded graph: %v", grap.String())
}
