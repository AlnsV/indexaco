package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"github.com/AlnsV/graph"
	"github.com/golang-collections/go-datastructures/queue"
	"log"
	"math"
	"os"
	"runtime"
	"time"
)

var (
	bestSolution   []int
	minimumIndex   int
	graphType      string
	testsSize      string
	multiThreading bool
	testIterations int
	typeParams     int
)

func init() {
	flag.StringVar(&graphType, "graph_type", "star", "--graph_type hypercube")
	flag.StringVar(&testsSize, "size", "opt", "--size small")
	flag.BoolVar(&multiThreading, "sequentially", true, "--sequentially true")
	flag.IntVar(&testIterations, "test_iters", 3, "--test_iters 3")
	flag.IntVar(&typeParams, "types", 1, "--types 3")
	flag.Parse()
}

func AntTour(
	ant Ant,
	g *graph.Mutable,
	output chan Ant,
	colony *Colony,
	pheromones *Pheromones,
	links map[Link]int) {

	antLinks := NewAntLinks(links)

	for antLinks.LinkCount > 0 {
		actualNode := ant.Vertex
		neighbors := g.Neighbors(actualNode)
		nextNode := SelectNextNode(actualNode, neighbors, ant.LinksTimesVisited, pheromones, colony.Alpha, colony.Beta)

		newLink := Link{actualNode, nextNode}
		if actualNode > nextNode {
			newLink = Link{nextNode, actualNode}
		}
		ant.LinksTimesVisited[newLink] += 1
		ant.VertexPath = append(ant.VertexPath, nextNode)

		if _, exist := antLinks.Links[newLink]; exist {
			delete(antLinks.Links, newLink)
			antLinks.LinkCount -= 1
		}

		ant.Vertex = nextNode
	}
	r := NewRouting(g.Order())
	LoadRoutingFromPath(ant.VertexPath, r, true)
	// PrintRouting(r)
	ant.Solution = r
	output <- ant
}

func RunAnts(colony *Colony, g *graph.Mutable, pheromones *Pheromones, links map[Link]int, Sequentially bool) {

	antExit := make(chan Ant)
	done := make(chan bool, 1)
	antQuantity := len(colony.Ants)

	// This tells us the maximum number of threads that the processor has
	// Will help us in multiprocessing ants
	maxActiveAnts := runtime.GOMAXPROCS(100)
	activeAnts := 0

	finishedCounter := 0
	go func() {
		for {
			select {
			case ant := <-antExit:
				EdgeIndex, _, LoadVector := GetRoutingStats(ant.Solution)
				if EdgeIndex == minimumIndex {

					bestSolution = CompareSolutions(bestSolution, LoadVector)
				}
				if EdgeIndex < minimumIndex || minimumIndex == 0 {
					minimumIndex = EdgeIndex
					bestSolution = LoadVector
					//log.Printf("Found a new best solution: index value: %d", EdgeIndex)
				}
				pheromones.Lock()
				UpdatePheromones(colony.Q, colony.Pho, EdgeIndex, pheromones.Table, ant.VertexPath)
				pheromones.Unlock()
				ant.Solution = Routing{}
				activeAnts--
				finishedCounter++
				if finishedCounter == antQuantity {
					done <- true
				}
			}
		}
	}()

	// Feature Switch for parallelism.
	// When Sequentially is false, this creates a queue of Ants
	// That will be started by batches of the maximum thread capacity that the
	// current processor has, allowing to keep the numbers of go routines stable at that quantity
	// and avoiding high memory consumption by creating just the necessary go routines at once
	// This approach use efficiently the power of the processor, every run should use around 95%+ of its capacity
	// This will depend of the use of third party apps on the current server/machine
	if Sequentially {
		for _, ant := range colony.Ants {
			AntTour(ant, g, antExit, colony, pheromones, CopyLinkIntMap(links))
		}
	} else {
		procsQueue := queue.New(int64(len(colony.Ants)))
		for _, ant := range colony.Ants {
			err := procsQueue.Put(ant)
			if err != nil {
				log.Fatal("Could'n put an element in the Queue")
			}
		}
		go func() {
			for {
				antsBatch, err := procsQueue.Get(int64(maxActiveAnts - activeAnts))

				if err != nil {
					log.Fatal("Could'n get an element from the Queue")
				}
				if len(antsBatch) > 0 {
					for _, ant := range antsBatch {
						go AntTour(ant.(Ant), g, antExit, colony, pheromones, CopyLinkIntMap(links))
						activeAnts++
					}
				}
			}
		}()
	}
	<-done
	// log.Printf("The minimum solution achieved so far is: %d", minimumIndex)
}

func SelectNextNode(
	ActualNode int, neighbors []int,
	linkLoad map[Link]int, pheromones *Pheromones,
	Alpha, Beta float64) int {
	var altValues []float64
	var weights []float64
	for _, vertex := range neighbors {
		tempLink := Link{ActualNode, vertex}
		// This is to be able to find links
		if ActualNode > vertex {
			tempLink = Link{vertex, ActualNode}
		}
		pheromones.RLock()
		vertexValue := getAlternativeValue(
			pheromones.Table[tempLink],
			Alpha,
			Beta,
			linkLoad[tempLink],
		)
		pheromones.RUnlock()
		altValues = append(altValues, vertexValue)
	}
	totalValue := Sum(altValues)
	for _, val := range altValues {
		weights = append(weights, val/totalValue)
	}
	return RandomWeightedChoice(neighbors, weights)
}

func getAlternativeValue(t, a, b float64, c int) float64 {
	// This prevent ZeroDivision due to no passing through the link previously
	if c == 0 {
		return math.Pow(t, a) * math.Pow(1/0.5, b)
	}
	return math.Pow(t, a) * math.Pow(1/float64(c), b)
}

func runIndexACO(cfg *TestCase, iter int) *Result {
	minimumIndex = 0
	bestSolution = []int{}
	fmt.Printf("Starting case: %s\n", cfg.Name)
	start := time.Now()
	g, links, pheromoneLinks, err := LoadStructureFromFile(".tgf", cfg.gType, cfg.gSize)
	if err != nil {
		log.Fatal(err)
	}
	pheromones := NewPheromones(pheromoneLinks)
	Colony := NewColony(cfg.Alpha, cfg.Beta, cfg.Pho, cfg.Q)
	for iter := 0; iter < cfg.MaxIterations; iter++ {
		Colony.Ants = LoadNodesToAnts(g)
		RunAnts(Colony, g, pheromones, links, cfg.Sequentially)
	}
	ExecDuration := time.Now().Sub(start).Seconds()
	fmt.Printf(
		"TestCase: %s, took %v seconds and found solution: %d\n",
		cfg.Name,
		ExecDuration,
		minimumIndex,
	)
	return NewResult(cfg.gType, cfg.gSize, iter, minimumIndex, ExecDuration)
}

func main() {
	file, err := os.Create(fmt.Sprintf("result_%s_%s_%d.csv", graphType, testsSize, typeParams))
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	writer := csv.NewWriter(file)
	err = writer.Write([]string{"tipo_grafo", "tam_grafo", "iteracion", "valor_indice", "tiempo_ejecucion"})
	if err != nil {
		log.Fatal(err)
	}
	for _, testCase := range BuildCases(multiThreading, graphType, testsSize, typeParams) {
		for i := 1; i <= testIterations; i++ {
			result := runIndexACO(testCase, i)
			err := writer.Write(result.AsStrings())
			if err != nil {
				log.Fatal(err)
			}
			writer.Flush()
		}
	}
	fmt.Println("test completed.")
}
