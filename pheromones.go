package main

import (
	"sync"
)

type Pheromones struct {
	sync.RWMutex
	Table map[Link]float64
}

func NewPheromones(table map[Link]float64) *Pheromones {
	return &Pheromones{Table: table}
}

func UpdatePheromones(Q, Pho float64, IndexValue int, pheromones map[Link]float64, antPath []int) {
	pathSize := len(antPath)
	for k := 0; k < pathSize; k++ {
		end := k + 2

		if end > pathSize {
			end = pathSize
		}
		path := antPath[k:end]
		if len(path) == 2 {
			org := path[0]
			dest := path[1]
			if org > dest {
				org, dest = dest, org
			}
			currentLink := Link{org, dest}
			prevValue := pheromones[currentLink]
			if IndexValue == 0 {
				IndexValue = 1
			}
			pheromones[currentLink] = (1-Pho)*prevValue + Q/float64(IndexValue)
		}
	}
}
