package main

import "fmt"

type Result struct {
	graphType  string
	numVertex  string
	iteration  int
	indexValue int
	ExecTime   float64
}

func NewResult(graphType string, numVertex string, iteration int, indexValue int, execTime float64) *Result {
	return &Result{graphType: graphType, numVertex: numVertex, iteration: iteration, indexValue: indexValue, ExecTime: execTime}
}

func (r *Result) AsStrings() []string {
	return []string{
		r.graphType,
		r.numVertex,
		fmt.Sprintf("%d", r.iteration),
		fmt.Sprintf("%d", r.indexValue),
		fmt.Sprintf("%.6f", r.ExecTime),
	}
}
