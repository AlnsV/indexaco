package main

import "fmt"

type Routing [][][]int

func NewRouting(size int) Routing {
	r := make(Routing, size)
	for i := range r {
		r[i] = make([][]int, size)
		for j := range r[i] {
			r[i][j] = make([]int, 0)
		}
	}
	return r
}

// O(n*n-i)
func LoadRoutingFromPath(antPath []int, routing Routing, Symmetric bool) {
	for idx, _ := range antPath {
		auxPath := antPath[idx:]
		auxSize := len(auxPath)
		for _, _ = range auxPath {
			elemPath := auxPath[:auxSize]
			elemPathSize := len(elemPath)
			firstVal := elemPath[0]
			lastVal := elemPath[elemPathSize-1]
			if firstVal != lastVal {

				prevPath := routing[firstVal][lastVal]
				prevReversePath := routing[lastVal][firstVal]
				if elemPathSize < len(prevPath) || len(prevPath) == 0 {
					routing[firstVal][lastVal] = elemPath
				}
				if Symmetric {
					if elemPathSize < len(prevReversePath) || len(prevReversePath) == 0 {
						routing[lastVal][firstVal] = elemPath
					}
				}
			}
			auxSize--
		}
	}
}

// O(N^2*T) + O(N)
func GetRoutingStats(routing Routing) (int, Link, []int) {

	loadCounter := map[Link]int{}
	for i := range routing {
		for j := range routing[i] {
			pathSize := len(routing[i][j])
			if pathSize > 2 {
				for k := 0; k < pathSize; k++ {
					end := k + 2

					if end > pathSize {
						end = pathSize
					}
					path := routing[i][j][k:end]
					if len(path) == 2 {
						org := path[0]
						dest := path[1]
						if org > dest {
							org, dest = dest, org
						}
						loadCounter[Link{org, dest}] += 1
					}
				}
			}
		}
	}
	maxLoad := 0
	var MostLoadedLink Link
	var LoadVector []int
	for link, load := range loadCounter {
		if load > maxLoad {
			maxLoad = load
			MostLoadedLink = link
		}
		LoadVector = append(LoadVector, load)
	}
	return maxLoad, MostLoadedLink, LoadVector
}

func PrintRouting(routing Routing) {
	for i := range routing {
		for j := range routing[i] {
			fmt.Printf("%v", routing[i][j])
		}
		fmt.Println()
	}
}
