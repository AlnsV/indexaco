package main

import (
	"fmt"
	"testing"
)

func TestNewRouting(t *testing.T) {
	r := NewRouting(4)
	fmt.Println(r)
}

func TestLoadRoutingFromPath(t *testing.T) {
	r := NewRouting(6)
	LoadRoutingFromPath([]int{0, 1, 2, 0, 1, 4, 5, 3, 4, 1, 2, 3}, r, false)
	PrintRouting(r)

	rTwo := NewRouting(6)
	LoadRoutingFromPath([]int{0, 2, 3, 5, 4, 1, 2, 3, 4, 5, 4, 1, 0}, rTwo, true)
	PrintRouting(rTwo)

	loadCounter := map[Link]int{}
	loadCounter[Link{2, 1}] += 1
	loadCounter[Link{3, 1}] += 1
	loadCounter[Link{2, 1}] += 1
	fmt.Println(loadCounter)
}

func TestCalculateEdgeForwardingIndex(t *testing.T) {
	r := NewRouting(6)
	LoadRoutingFromPath([]int{0, 1, 2, 0, 1, 4, 5, 3, 4, 1, 2, 3, 4, 1, 0, 2, 3}, r, false)
	PrintRouting(r)
	value, LinkLoaded, _ := GetRoutingStats(r)
	fmt.Println(value, LinkLoaded)
	rTwo := NewRouting(6)
	LoadRoutingFromPath([]int{0, 2, 3, 5, 4, 1, 2, 3, 4, 5, 4, 1, 0}, rTwo, false)
	PrintRouting(rTwo)
	value, LinkLoaded, _ = GetRoutingStats(rTwo)
	fmt.Println(value, LinkLoaded)
}
