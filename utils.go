package main

import "math/rand"

func Sum(data []float64) float64 {
	result := 0.0
	for _, v := range data {
		result += v
	}
	return result
}

// RandomWeightedChoice es una Implementacion del metodo de transformacion inversa
func RandomWeightedChoice(Alias []int, Weights []float64) int {
	u := rand.Float64()
	cumulativeWeight := 0.0
	for idx, weight := range Weights {
		if cumulativeWeight > 0 {
			if u < cumulativeWeight+weight {
				return Alias[idx]
			}
		} else {
			if u < weight {
				return Alias[idx]
			}
		}
		cumulativeWeight += weight
	}
	return -1
}

func CopyLinkIntMap(data map[Link]int) map[Link]int {
	newOne := make(map[Link]int)
	for k, v := range data {
		newOne[k] = v
	}
	return newOne
}
